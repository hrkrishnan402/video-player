
import 'package:flutter/material.dart';
import 'package:flutter_otp/flutter_otp.dart';
import 'package:videoplayer/videoplayer.dart';
class OTPScreen extends StatefulWidget {

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  bool phoneNumberSubmitted = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: !phoneNumberSubmitted ? Column(children: [
      TextFormField(decoration: InputDecoration(labelText: "Enter Phone Number"),),
      RaisedButton(child: Text("Send OTP"), onPressed: (){
        setState(() {
                phoneNumberSubmitted = true;  
                });
      },) 
    ],) : 
      TextFormField(
        onChanged: (val){
        if( val == "1234" ){ 
          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => VideoPlayerApp()),
  ); }
        },
      decoration:InputDecoration(labelText: "Enter OTP"),));

  }
}