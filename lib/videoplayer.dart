
import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:path_provider/path_provider.dart';
import 'package:videoplayer/main.dart';


class VideoPlayerApp extends StatelessWidget {
  const VideoPlayerApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  VideoPlayerScreen();
  }
}

class VideoPlayerScreen extends StatefulWidget {
  const VideoPlayerScreen({Key key}) : super(key: key);

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
   VideoPlayerController _controller;
   Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    createDirectory();
    _controller = VideoPlayerController.network(
      // 'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4'
    );


    // Initialize the controller and store the Future for later use.
    _initializeVideoPlayerFuture = _controller.initialize();

    // Use the controller to loop the video.
    _controller.setLooping(true);

    super.initState();
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Butterfly Video'),
      ),
      body: Stack(
        children: [
        FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: VideoPlayer(_controller),
            );
          } else {
            // If the VideoPlayerController is still initializing, show a
            // loading spinner.
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ), 
      Positioned(
        bottom: 30,
        left: 10,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
          GestureDetector(
        onTap: () {
          // Wrap the play or pause in a call to `setState`. This ensures the
          // correct icon is shown.
          setState(() {
            // If the video is playing, pause it.
            if (_controller.value.isPlaying) {
              _controller.pause();
            } else {
              // If the video is paused, play it.
              _controller.play();
            }
          });
        },
        // Display the correct icon depending on the state of the player.
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow, size: 40,
          color: Colors.white70,
        ),
      ),
      SizedBox(width: 30,),
        buildButton(Icon(Icons.replay_5,size: 40, color: Colors.white70,), rewind5Seconds),
        SizedBox(width: 30,),
        buildButton(Icon(Icons.forward_5, size: 40, color: Colors.white70,), forward5Seconds),
        SizedBox(width: 30,),
        GestureDetector(
          child: Icon(Icons.download_rounded, size: 40, color: Colors.white70,),
          onTap: ()async{
            downloadFile("https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
          },
        ),
        SizedBox(width: 30,),
        buildButton(
          Icon(Icons.volume_mute, size: 40, color: Colors.white70,), muteVolume),
        ],)
      )
      ],)
    );
  }
  Widget buildButton(Widget child, Function onPressed) => Container(
        child: GestureDetector(
          child: child,
          onTap: onPressed,
        ),
      );

Future forward5Seconds() async =>
      goToPosition((currentPosition) => currentPosition + Duration(seconds: 5));

Future rewind5Seconds() async =>
      goToPosition((currentPosition) => currentPosition - Duration(seconds: 5));

Future muteVolume() async {
  _controller.value.volume == 0.0 ? 
  setState((){
    _controller.setVolume(5.0);
  }) :
  setState(() {
      _controller.setVolume(0.0);
    });
}

  Future goToPosition(
    Duration Function(Duration currentPosition) builder,
  ) async {
    final currentPosition = await _controller.position;
    final newPosition = builder(currentPosition);

    await _controller.seekTo(newPosition);
  }
}



Future downloadFile(String url) async {
  Dio dio = Dio();

  try {
    var dir = await getApplicationDocumentsDirectory();
    await dio.download(url, "${dir.path}/myFile.txt", onReceiveProgress: (rec, total) {
      print("Rec: $rec , Total: $total");
    });
  } catch (e) {
    print(e);
  }
  print("Download completed");
}

Future fileExists(String url) async{
String fileName = url;
String dir = (await getApplicationDocumentsDirectory()).path;
String savePath = '$dir/$fileName';

//for a directory: await Directory(savePath).exists();
if (await File(savePath).exists()) {
    print("File exists");        
} else {
    print("File don't exists");
}}
