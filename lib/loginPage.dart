import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:videoplayer/otpValidation.dart';
import 'package:videoplayer/videoplayer.dart';

class LoginPage extends StatelessWidget {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        TextFormField(controller: emailController,decoration: InputDecoration(labelText: "Email-id"),),
        TextFormField(controller: passwordController,decoration: InputDecoration(labelText: "Password"),obscureText: true,),
        RaisedButton(onPressed: () => emailLoginValidation(emailController.text, passwordController.text, context), child: Text("Login"),),
        RaisedButton(child: Text("Login with Phone Number"), onPressed: () => Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => OTPScreen()),
  ),)
      ],),
    );
  }

  void emailLoginValidation(String email, String password, context){
    if(email == "hrkrishnan402@gmail.com" && password == "1234"){
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => VideoPlayerApp()),
  );
    }
  }


}